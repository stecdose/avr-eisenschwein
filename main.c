/* a very basic implementation to generate alternating
 * complementary signals, aka push-pull.
 * 
 * no interrupts are used, just a single endless loop doing the waiting
 * and setting/clearing of IOs.
 * 
 * It is more or less hardcoded for 50hz with a small deadtime
 * to drive a simple center tapped 12Vx2-230V transformer.
 * For 60hz operation period and on_time must be shortened by a few %
 *
 * I have chosen PB3 and PB4, because they do not interfer with ISP,
 * this could be quite dangerous with a big transformer connected.
 * A simple AND gate would allow operation on dedicated PWM pins but being
 * locked while in RESET (ISP mode is with reset active).
 * 
 *  
 */
#include <avr/io.h>
#include <util/delay.h>

#define enQ1()  	PORTB |= (1<<PB3)  
#define enQ2()  	PORTB |= (1<<PB4)
#define disQ1() 	PORTB &= ~(1<<PB3)
#define disQ2() 	PORTB &= ~(1<<PB4)
#define disQ12() 	PORTB &= ~((1<<PB3)|(1<<PB4))

void my_delay(uint8_t d) {
    uint16_t cnt = d * 46;

    while(cnt) {
        asm("nop");
        cnt--;
    }
}

int main(int argc, char **argv) {
    disQ12();

    DDRB = (1<<PB3)|(1<<PB4);

    uint16_t period = 260;                  // keep!
    uint16_t on_time = 240;    		 		// 0-250
    uint16_t off_time = period-on_time;

    while (1) {
            enQ1();
            my_delay(on_time);
            disQ1();
            my_delay(off_time);
            enQ2();
            my_delay(on_time);
            disQ2();
            my_delay(off_time);
	}
}
